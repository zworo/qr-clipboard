window.addEventListener("load", function (event) {
  new Vue({
    el: '#app',
    components: {
      'app': window.httpVueLoader('src/components/App.vue'),
    },
  });
  Vue.use(Toasted, {
    theme: "outline",
    position: 'bottom-right',
    duration: 5000
  })
});