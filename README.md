# qr-clipboard

> Scan (or generate) QR code and copy content to clipboard. Useful to share texts between devices

## Why
I missed a tool to easily transfer text from a phone to a laptop. Especially for complex, long password texts from KeePass.

## Used libraries
* vue.js (https://vuejs.org/)
* http-vue-loader (https://github.com/FranckFreiburger/http-vue-loader)
* clipboard.js (https://clipboardjs.com/)
* Instascan (https://schmich.github.io/instascan/)
* vue-toasted (https://github.com/shakee93/vue-toasted)
* Simple Grid (https://simplegrid.io/)
* Font Awesome (https://fontawesome.com/)

## Todo
* automatic clipboard cleanup after some time